 close all

%% import data
data = importfile('data.txt');

dT = 1/100;

%% initial conditions
g = 1;
phi = deg2rad(0.803);
theta = deg2rad(-1.298);
psi = deg2rad(276.628);
omx = 0;
omy = deg2rad(0);
omz = deg2rad(0);
bx = 0;
by = 0;
bz = 0;

% state vector
x_hat = [phi theta psi omx omy omz bx by bz]';

%% KALMAN LOOP
% number of iterations = number of data samples
N = size(data(:,1),1);
t = 0:dT:(N-1)*dT;
P = eye(9).*10;

%        phi  theta  psi     omx          omy           omz               bx              by              bz
Qc = [    0.0000001     0     0       0       0             0                0               0               0;
          0     0.0000001     0       0       0             0                0               0               0;
          0     0     0.0000001       0       0             0                0               0               0;
          0     0     0   deg2rad(0.3)       0             0                0               0               0;
          0     0     0       0        deg2rad(0.3)        0                0               0               0;
          0     0     0       0            0         deg2rad(.3)           0               0               0;
          0     0     0       0            0             0            deg2rad(1e-9)          0               0;
          0     0     0       0            0             0                0           deg2rad(1e-9)          0;
          0     0     0       0            0             0                0               0           deg2rad(1e-0)];  
 
%         ax         ay         az          omx           omy          omz       mgx  mgy  mgz 
Rc = [    (20e-3)^2      	 0          0            0             0            0         0    0    0;
          0          (20e-3)^2          0            0             0            0         0    0    0;
          0          0          (40e-3)^2            0             0            0         0    0    0;
          0          0          0        deg2rad(1)^2        0            0         0    0    0;
          0          0          0            0         deg2rad(1)^2       0         0    0    0;
          0          0          0            0             0        deg2rad(2)^2    0    0    0;
          0          0          0            0             0            0         1    0    0;
          0          0          0            0             0            0         0    1    0;
          0          0          0            0             0            0         0    0    1];

Rk = Rc.*dT;

% arrays to store progress values
xx = zeros(9,N);
inno = zeros(9,N);
Kk = zeros(N,9,9);

% estimate
for i=1:N
    % get values from state vector
    phi     = x_hat(1);
    theta   = x_hat(2);
    psi     = x_hat(3);
    omx     = x_hat(4);
    omy     = x_hat(5);
    omz     = x_hat(6);
    bx      = x_hat(7);
    by      = x_hat(8);
    bz      = x_hat(9);
    
    % update jacobians
    J_A = [omy*cos(phi)*tan(theta)-omz*sin(phi)*tan(theta), omz*cos(phi)/cos(theta)^2+omy*sin(phi)/cos(theta)^2                      , 0, 1, sin(phi)*tan(theta), cos(phi)*tan(theta), 0, 0, 0;
           -omz*cos(phi)-omy*sin(phi)                     , 0                                                                        , 0, 0, cos(phi)           , -sin(phi)          , 0, 0, 0;
           omy*cos(phi)/cos(theta)-omz*sin(phi)/cos(theta), omz*(cos(phi)/cos(theta))*tan(theta)+omy*(sin(phi)/cos(theta))*tan(theta), 0, 0, sin(phi)/cos(theta), cos(phi)/cos(theta), 0, 0, 0;
           zeros(6, 9)];
    
    J_H = [0                     , -g*cos(theta)         , 0, 0, 0, 0, 0, 0, 0;
           g*cos(phi)*cos(theta) , -g*sin(phi)*sin(theta), 0, 0, 0, 0, 0, 0, 0;
           -g*sin(phi)*cos(theta), -g*cos(phi)*sin(theta), 0, 0, 0, 0, 0, 0, 0;
           0                     , 0                     , 0, 1, 0, 0, 1, 0, 0;
           0                     , 0                     , 0, 0, 1, 0, 0, 1, 0;
           0                     , 0                     , 0, 0, 0, 1, 0, 0, 1;
           zeros(3,9)];
       
    PHI = eye(9) + dT.*J_A;

    Qk = 0.5.*dT.*(PHI*Qc + Qc*PHI');
    
    % compute the kalman gain
    K = (P*J_H')/(J_H*P*J_H'+Rk);
    Kk(i,:,:) = K;
    
    % update & correct the estimate with the measurement
    z = [data(i,7:9), deg2rad(data(i,4:6)), zeros(1,3)]';
    
    ax = -g*sin(theta);
    ay = g*sin(phi)*cos(theta);
    az = g*cos(phi)*cos(theta);
    
    z_hat = [ax, ay, az, omx+bx, omy+by, omz+bz, 0 0 0]';
    
    inno(:,i) = z - z_hat;
    x_hat = x_hat + K*(inno(:,i));
    
    % store iteration state vector
    xx(:,i) = x_hat;
       
    % compute the error covariance matrix for the updated estimate
    P = (eye(9)-K*J_H)*P;
    
    % project the state estimate one step ahead
    x_hat = PHI*x_hat;
    P = PHI*P*PHI' + Qk;
   
    P = (P+P')/2;
end

%% show results

%% plot angles 
figure('name', 'Estimated angles')
subplot(2,1,1)
plot(t,data(:,2))
hold on
plot(t,rad2deg(xx(1,:)))
hold off
grid on;
title('Pitch estimation')
xlabel('time [s]') % x-axis label
ylabel('value estimate') % y-axis label

subplot(2,1,2)
plot(t,data(:,3))
hold on
plot(t,rad2deg(xx(2,:)))
hold off
grid on
title('Roll estimation')
xlabel('time [s]') % x-axis label
ylabel('value estimate') % y-axis label

% subplot(3,1,3)
% plot(data(:,1))
% hold on
% plot(rad2deg(xx(3,:)))
% hold off
% title('Hdg estimation')
% xlabel('sample [-]') % x-axis label
% ylabel('value estimate') % y-axis label

%% plot biases
figure('name', 'Biases')
hold on
subplot(2,1,1)
plot(t,xx(7,:), 'LineWidth', 1)
title('Bias (X axis)')
xlabel('time [s]')
grid on;

subplot(2,1,2)
plot(t,xx(8,:), 'LineWidth', 1)
title('Bias(Y axis)')
xlabel('time [s]')
grid on;



%% plot inovations of Acc
name = sprintf('%.3f, %.3f, %.3f',mean(inno(1,:)),mean(inno(2,:)),mean(inno(3,:)));
figure('name', name)
subplot(3,1,1)
plot(t,inno(1,:), 'LineWidth', 1)
title('Innovations of AXL (X axis)')
xlabel('time [s]')
grid on;

subplot(3,1,2)
plot(t,inno(2,:), 'LineWidth', 1)
title('Innovations of AXL (Y axis)')
xlabel('time [s]')
grid on;

subplot(3,1,3)
plot(t,inno(3,:), 'LineWidth', 1)
title('Innovations of AXL (Z axis)')
xlabel('time [s]')
grid on;

%% plot innovations of gyro
figure('name', 'Inovations of gyros')
subplot(3,1,1)
plot(t,inno(4,:), 'LineWidth', 1)
title('Innovations of GYR (X axis)')
xlabel('time [s]')
grid on;

subplot(3,1,2)
plot(t,inno(5,:), 'LineWidth', 1)
title('Innovations of GYR (Y axis)')
xlabel('time [s]')
grid on;

subplot(3,1,3)
plot(t,inno(6,:), 'LineWidth', 1)
title('Innovations of GYR (Z axis)')
xlabel('time [s]')
grid on;

%% plot K
figure('name', 'Kalman gain')

for ii=1:6
    subplot(6,1,ii);
    summ = Kk(:,1,ii) + Kk(:,2,ii)+Kk(:,3,ii)+Kk(:,4,ii)+Kk(:,5,ii)+Kk(:,6,ii);
    plot(t,summ);
    grid on;
end

